import numpy as np
from scipy import sparse
from scipy.sparse.linalg import eigsh
import matplotlib.pyplot as plt

from square_well import SquareWellHamiltonian


def main():
    a = 5
    b = a / 2.0
    nrep = 1
    L = nrep * a

    nk = 51
    nbands = 10

    H = SquareWellHamiltonian(L=L, V0=3.0, N=501 * nrep, nrep=nrep)
    # k between -pi/L and pi/L

    # some test cases below. for real work import the Hamiltonian and solution classes.
    # to a different file and don't touch this file
    print("Don't directly run this file except for testing.")

    if 1:
        H.construct_hamiltonian(k=0.0, b=b)
        eigenvalues, eigenfunctions = eigsh(H.H, 7, which="SA")
        print(eigenvalues)
        print("expect: 0.4408, 1.6998, 3.0488, 4.5932, ...")

    if 0:
        H.construct_hamiltonian(k=0.0, b=b, A0=-0.5, icell=3)
        eigenvalues, eigenfunctions = eigsh(H.H, 7, which="SA")
        print(eigenvalues)

    if 0:
        ks = np.linspace(-np.pi / L, np.pi / L, nk)
        e_nk = np.zeros((nbands, nk))

        V = H.kronig_penney_potential(b)
        for ik, k in enumerate(ks):
            print(f"{ik+1}/{nk}")
            # H.construct_hamiltonian(k=k, b=b)
            T = H.kinetic_energy(k)
            H.H = sparse.csr_matrix(T + V)
            eigenvalues = eigsh(H.H, nbands, which="SA", return_eigenvectors=False)
            e_nk[:, ik] = eigenvalues

        if 0:

            for i in range(nbands):
                plt.plot(ks[nk // 2 :], e_nk[i, nk // 2 :])
            plt.show()


if __name__ == "__main__":
    main()
