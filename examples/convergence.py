# import sys
# sys.path.insert(0, "")
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import eigsh

from square_well import SquareWellHamiltonian, SquareWellData


def check_nbasis_convergence(input: dict, nbands):
    nrep = input["nrep"]
    nbasis_max = 1000
    k = 0.1 * np.pi / (input["a"] * nrep)

    # reference
    H = SquareWellHamiltonian(
        L=input["a"] * nrep, V0=input["V0"], N=int(nrep * 1.5 * nbasis_max), nrep=nrep
    )
    H.construct_hamiltonian(k=k, b=input["b"], A0=input["A0"], icell=input["icell"])
    eigenvalues_ref = eigsh(H.H, nbands, which="SA", return_eigenvectors=False)

    for nbasis in range(50 * nrep, nbasis_max * nrep, 50):
        H = SquareWellHamiltonian(
            L=input["a"] * nrep, V0=input["V0"], N=nbasis, nrep=nrep
        )
        H.construct_hamiltonian(k=k, b=input["b"], A0=input["A0"], icell=input["icell"])
        eigenvalues = eigsh(H.H, nbands, which="SA", return_eigenvectors=False)
        if eigenvalues_ref is None:
            eigenvalues_ref = eigenvalues.copy()

        deviation = np.max(np.abs(eigenvalues_ref - eigenvalues))
        print(f"{nbasis/nrep:.1f} x {nrep} = {nbasis}: max_delta = {deviation:.5f}")


def main():
    # input parameters
    input = {
        "a": 5,  # periodicity of potential
        "b": None,  # width of zero potential part
        "nrep": 5,  # number of unit cells
        "V0": 3.0,  # square well height
        "icell": None,
        "A0": None,
    }
    # if quadratic perturbation:
    if 0:
        input["icell"] = 3  # unit cell number for perturbation
        input["A0"] = 0.5  # amplitude of perturbation

    nk = 101 // input["nrep"]  # number of kpoints to sample
    nbands = 8 * input["nrep"]  # number of eigenvalues required

    # result: At least 600 points per unit cell for absolute error < 0.01
    check_nbasis_convergence(input, nbands)


if __name__ == "__main__":
    main()
