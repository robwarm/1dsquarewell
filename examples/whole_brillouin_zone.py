# import sys
# sys.path.insert(0, "")
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import eigsh

from square_well import SquareWellHamiltonian, SquareWellData


def scan_brillouin_zone(input: dict, nk, nbands, nbasis):
    L = input["a"] * input["nrep"]
    ks = np.linspace(0, np.pi / (L), nk)

    e_nk = np.zeros((nbands, nk))
    ev_xnk = np.zeros((nbasis - 1, nbands, nk), dtype=complex)

    H = SquareWellHamiltonian(L=L, V0=input["V0"], N=nbasis, nrep=input["nrep"])
    V = H.kronig_penney_potential(input["b"])
    if input["A0"] is not None and input["icell"] is not None:
        V += H.perturbation_potential(input["A0"], input["icell"], input["b"])
    for ik, k in enumerate(ks):
        print(f"{ik+1}/{nk}")
        T = H.kinetic_energy(k)
        H.H = sparse.csr_matrix(T + V)
        eigenvalues, eigenvectors = eigsh(
            H.H, nbands, which="SA", return_eigenvectors=True
        )
        idx = np.argsort(eigenvalues)
        # need to sort those buggers

        e_nk[:, ik] = eigenvalues[idx]
        ev_xnk[:, :, ik] = eigenvectors[:, idx]

    return SquareWellData(
        e_nk, ev_xnk, ks, np.linspace(0, L, nbasis)[:-1], V.diagonal()
    )


def main():
    # input parameters
    input = {
        "a": 5,  # periodicity of potential
        "b": None,  # width of zero potential part
        "nrep": 1,  # number of unit cells
        "V0": 3.0,  # square well height
        "icell": None,
        "A0": None,
    }
    # if quadratic perturbation:
    if 0:
        input["icell"] = 3  # unit cell number for perturbation
        input["A0"] = 0.5  # amplitude of perturbation

    nk = 101 // input["nrep"]  # number of kpoints to sample
    nbands = 6 * input["nrep"]  # number of eigenvalues required

    nbasis = 651 * input["nrep"]  # number of lattice points

    # Get energies and wavefunctions over the Brillouin zone
    if 1:
        data = scan_brillouin_zone(input, nk, nbands, nbasis)
        # best to store this data.
        data.save("test.pckl")
    else:
        data = SquareWellData.load("test.pckl")

    # plot some stuff
    import matplotlib.pyplot as plt

    plt.figure()
    plt.title("Potential vs x")
    plt.plot(data.xs, data.potential)
    plt.show(block=False)

    plt.figure()
    plt.title("Band structure")
    for e_k in data.eigenvalues:
        plt.plot(data.klist, e_k)
    plt.show(block=False)

    tau = data.kinetic_energy_density(4, 5)
    sum_tau = data.total_kinetic_energy_density(input["V0"])
    sum_rho = data.total_wavefunction_density(input["V0"])
    plt.figure()
    plt.title("Total density and kinetic energy density for E<=V0")
    # plt.plot(data.xs, tau)
    plt.plot(data.xs, sum_tau, label="tau")
    plt.plot(data.xs, sum_rho, label="rho")
    plt.show()

    # print(2 occupied bands per nrap)
    print(data.number_of_states(input["V0"]))


if __name__ == "__main__":
    main()
