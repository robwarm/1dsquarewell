"""
Code solve 1D periodic square well potential on a grid
"""

from dataclasses import dataclass
import numpy as np
import pickle
from scipy import sparse
from typing import Optional


class SquareWellHamiltonian:
    """
    Hamiltonian for periodic 1D finite square wells

    hbar = 1
    m = 1

    Note: For periodic bondary conditions the first and last point are the same
    so the last point needs to be omitted
    """

    def __init__(self, L: float, V0: float, N: int, nrep: int = 1) -> None:
        self.N = N  # total number of gridpoints
        self.L = L  # length of super cell
        self.h = self.L / (self.N - 1.0)  # grid spacing
        self.x = np.linspace(0, self.L, self.N)
        assert abs(self.h - (self.x[1] - self.x[0])) < 1e-12
        self.V0 = V0  # square well depth
        self.nrep = nrep  # number of unit cells

        self.H = None  # hamiltonian matrix

    def kinetic_energy(self, k: float) -> np.ndarray:
        """
        kinetic energy contribution

        Varga and Driscoll Computational Nanoscience
        Equation 4.9
        """
        h2m = 0.5  # hbar**2 / (2*m)
        # print(self.N)
        T = np.zeros((self.N - 1, self.N - 1), dtype=complex)
        np.fill_diagonal(T, (2.0 / self.h**2 + k**2))
        offdiag_factor = -1.0 * (1.0 / self.h**2 - 1j * k / self.h)
        np.fill_diagonal(T[1:], np.conj(offdiag_factor))
        np.fill_diagonal(T[:, 1:], offdiag_factor)
        # wrap last points (sign checked)
        T[0, -1] = np.conj(offdiag_factor)
        T[-1, 0] = offdiag_factor
        return T * h2m

    def kronig_penney_potential(self, b: float | None = None) -> np.ndarray:
        """
        Kronig-Penney potential, zero at centre of cell

        Varga and Driscoll Computational Nanoscience
        Equation 4.11

        Parameters:
        -----------
        b: float
        Length of zero potential part, if None b=a/2
        """

        a = self.L / self.nrep
        if b is None:
            b = a / 2.0

        tmp = np.zeros((self.N), dtype=float)

        for icell in range(self.nrep):
            # left side
            mask1 = np.where(self.x < (icell * a + (a - b) / 2))[0]
            mask2 = np.where(self.x >= (icell * a))[0]
            mask_left = np.intersect1d(mask1, mask2)
            # right side
            mask1 = np.where(self.x > (icell * a + (a + b) / 2))[0]
            mask2 = np.where(self.x <= ((icell + 1) * a))[0]
            mask_right = np.intersect1d(mask1, mask2)
            mask = np.union1d(mask_left, mask_right)
            tmp[mask] = self.V0

        V = np.zeros((self.N - 1, self.N - 1), dtype=float)
        np.fill_diagonal(V, tmp[:-1])
        return V

    def perturbation_potential(
        self, A0: float, icell: int, b: float | None = None
    ) -> np.ndarray:
        """Add A*x^2 potential to selected unit cell.

        Potential is zero at heavy side points and maximal at center.
        Note: In this definition normalises such that maximum is always A

        Postive A will yield bump
        Negative A will yield through
        """

        a = self.L / self.nrep
        if b is None:
            b = a / 2.0
        # indices in icell with V0=0
        mask1 = np.where(self.x >= (icell * a + (a - b) / 2))[0]
        mask2 = np.where(self.x <= (icell * a + (a + b) / 2))[0]
        mask_cell = np.intersect1d(mask1, mask2)
        # center of that cell
        xcenter = (self.x[mask_cell[0]] + self.x[mask_cell[-1]]) / 2.0
        cellx = self.x[mask_cell] - xcenter

        cell_pot = A0 * (1.0 - (cellx / cellx[0]) ** 2)
        tmp = np.zeros((self.N), dtype=float)
        tmp[mask_cell] = cell_pot
        V = np.zeros((self.N - 1, self.N - 1), dtype=float)
        np.fill_diagonal(V, tmp[:-1])
        return V

    def construct_hamiltonian(
        self, k: float, b: float, A0: float | None = None, icell: int | None = None
    ):
        """Returns T+V0+Vper as sparse matrix"""
        T = self.kinetic_energy(k)
        # print(T)
        V = self.kronig_penney_potential(b)
        if A0 is not None and icell is not None:
            V += self.perturbation_potential(A0, icell, b)

        # print(V.shape, self.x.shape)
        if 0:
            import matplotlib.pyplot as plt

            plt.plot(self.x, V.diagonal())
            plt.show()
        self.H = sparse.csr_matrix(T + V)


@dataclass
class SquareWellData:
    eigenvalues: np.ndarray
    eigenfunctions: np.ndarray
    klist: np.ndarray
    xs: np.ndarray
    potential: Optional[np.ndarray] = None

    def wavefunction(self, n: int, k: int) -> np.ndarray:
        """Returns eigenfunctions / sqrt(h) times exp(ikx)"""
        return self.wavefunction_unk(n, k) * np.exp(1j * self.xs * self.klist[k])

    def wavefunction_unk(self, n: int, k: int) -> np.ndarray:
        """Return eigenfunction / sqrt(h), so u_nk(x)"""
        return self.eigenfunctions[:, n, k] / np.sqrt(self._h)

    def save(self, filename: str):
        """Save data as .pckl file"""
        with open(filename, "wb") as file_:
            pickle.dump(self, file_, -1)

    @classmethod
    def load(cls, filename: str):
        """Load .pckl file"""
        return pickle.load(open(filename, "rb", -1))

    def wavefunction_derivative(self, n: int, k: int) -> np.ndarray:
        """
        d/dx Psi_nk(x) = ik exp(ikx) u_nk(x) + exp(ikx) d/dx u_nk(x)
                         (ik u_nk(x) + d/dx u_nk(x)) * exp(ikx)

        d/dx u(x) approximated as (u(x+1) - u(x-1))/ (2h) incl PBC
        """
        # PBC: u_nk[0] = u_nk[-1], but we defined grid to exclude repeated point
        # central difference derivative
        u = self.wavefunction_unk(n, k)
        uprime = np.zeros_like(u)
        uprime[1:-1] = u[2:] - u[:-2]
        uprime[0] = u[1] - u[-1]
        uprime[-1] = u[0] - u[-2]
        uprime /= 2.0 * self._h

        uprime += 1j * self.klist[k] * u

        return uprime * np.exp(1j * self.klist[k] * self.xs)

    def kinetic_energy_density(self, n: int, k: int) -> np.ndarray:
        """kinetic energy density using central difference derivatives

        tau(n,k) = 1/2 |d/dx Psi_nk(x)|^2
        """
        # hbar**2 / (2*m) = 1/2
        return 0.5 * np.abs(self.wavefunction_derivative(n, k)) ** 2

    @property
    def _kweight(self) -> np.ndarray:
        """BZ goes from [-pi/L to pi/L), but IBZ goes from 0 to pi/L

        All points except 0 and pi in the IBZ therefore represent
        two points and count double.
        """
        assert abs(self.klist[0]) < 1e-6
        L = self._h * len(self.xs)  # remember pbc
        assert abs(self.klist[-1] - np.pi / L) < 1e-6

        kweight = 2 * np.ones_like(self.klist)
        kweight[0] = 1.0
        kweight[-1] = 1.0
        return kweight

    @property
    def _h(self) -> float:
        return self.xs[1] - self.xs[0]

    def total_kinetic_energy_density(self, Emax: float) -> np.ndarray:
        """Returns the sum of the kinetic energy densities until Emax"""
        sum_tau = np.zeros(len(self.xs))

        for k in range(len(self.klist)):
            for n in range(len(self.eigenvalues)):
                if self.eigenvalues[n, k] > Emax:
                    break
                sum_tau += self.kinetic_energy_density(n, k) * self._kweight[k]
        return sum_tau / sum(self._kweight)

    def wavefunction_density(self, n: int, k: int) -> np.ndarray:
        """returns |Psi_nk|**2"""
        return np.abs(self.wavefunction_unk(n, k)) ** 2

    def total_wavefunction_density(self, Emax: float) -> np.ndarray:
        """Returns the sum of the wavefunction densities until Emax"""
        sum_rho = np.zeros(len(self.xs))
        for k in range(self.eigenvalues.shape[1]):
            for n in range(self.eigenvalues.shape[0]):
                if self.eigenvalues[n, k] > Emax:
                    break
                # print(n, k, self.eigenvalues[n, k])
                sum_rho += self.wavefunction_density(n, k) * self._kweight[k]
        return sum_rho / sum(self._kweight)

    def number_of_states(self, Emax: float) -> float:
        """Returns the number of states below Emax

        Times 2 would be number of electrons.
        """
        sum_rho = self.total_wavefunction_density(Emax)

        return sum(sum_rho) * self._h

    def potential_energy_density(self, n: int, k: int) -> np.ndarray:
        """Returns potential energy density Psi* V(X) Psi"""
        return self.wavefunction_density(n, k) * self.potential
        # return (psi.conj() * self.potential * psi).real

    def total_potential_energy_density(self, Emax: float) -> np.ndarray:
        """Returns potential energy density Psi* V(X) Psi for total
        wave function density."""
        return self.total_wavefunction_density(Emax) * self.potential
