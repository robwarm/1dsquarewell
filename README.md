# 1dsquarewell

## Description
This is a simple educational code which implements the 1D finite square well with periodic boundary conditions.


## Test and Deploy
To be done
Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

***


## Usage
export PYTHONPATH="/path_to_code/1dsquarewell/squarewell/":$PYTHONPATH


## Authors and acknowledgment
Authors:
Dr Robert Warmbier, University of the Witwatersrand, Johannesburg

